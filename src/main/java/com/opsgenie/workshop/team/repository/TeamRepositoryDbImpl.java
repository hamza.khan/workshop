package com.opsgenie.workshop.team.repository;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.opsgenie.workshop.team.Team;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
@Qualifier("TeamRepositoryDbImpl")
public class TeamRepositoryDbImpl implements TeamRepository {

    @Value("${aws.secretKey}")
    private String secretKey;
    @Value("${aws.accessKey}")
    private String accessKey;
    @Value("${aws.url:http://localhost:3966}")
    private String url;
    @Value("${aws.region:us-west-2}")
    private String region;

    private AWSCredentials awsCredentials;
    private AWSCredentialsProvider awsCredentialsProvider;
    private AmazonDynamoDB client;
    private DynamoDB dynamoDB;
    private Table table;

    @PostConstruct
    void postConstruct() {
        awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);
        client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration(url, region))
                .withCredentials(awsCredentialsProvider).build();
        dynamoDB = new DynamoDB(client);
        table = dynamoDB.getTable("onboarding_team");
    }

    @Override
    public List<Team> listTeams() {
        List<Team> teams = new ArrayList<>();

        ScanRequest scanRequest = new ScanRequest()
                .withTableName("onboarding_team");

        ScanResult result = client.scan(scanRequest);
        for (Map<String, AttributeValue> item : result.getItems()) {
            Team team = new Team();

            team.setId(item.get("id").getS());

            if (item.get("name") != null)
                team.setName(item.get("name").getS());

            if (item.get("userIds") != null) {
                List<AttributeValue> tempList = item.get("userIds").getL();
                List<String> list = new ArrayList<>();
                for (AttributeValue value : tempList) {
                    list.add(value.getS());
                }
                team.setUserIds(list);
            }

            teams.add(team);
        }

        return teams;
    }

    @Override
    public Team add(Team team) {
        Team addTeam = new Team(team);

        Table table = dynamoDB.getTable("onboarding_team");

        Item item = teamToItem(addTeam);

        table.putItem(item);

        return addTeam;
    }

    @Override
    public Team delete(String teamId) {
        Table table = dynamoDB.getTable("onboarding_team");

        Item item = table.getItem(new PrimaryKey("id", teamId));

        Team team = itemToTeam(item);

        table.deleteItem(new PrimaryKey("id", teamId));

        return team;
    }

    @Override
    public Team get(String id) {
        Item item = table.getItem(new PrimaryKey("id", id));

        if (item == null)
            return null;

        return itemToTeam(item);
    }

    @Override
    public Team update(Team team) {
        delete(team.getId());

        Team addedTeam = add(team);

        return addedTeam;
    }

    public Team addMember(String userId, String teamId) {

        Team team = get(teamId);

        if (team == null)
            return null;

        team.getUserIds().add(userId);

        Team updatedTeam = update(team);

        return updatedTeam;
    }

    public static Team itemToTeam(Item item) {
        Team team = new Team();

        team.setId(item.getString("id"));

        if (item.getList("userIds") != null)
            team.setUserIds(item.getList("userIds"));

        if (item.getString("name") != null)
            team.setName(item.getString("name"));

        return team;
    }

    public static Item teamToItem(Team addTeam) {
        Item item = new Item();
        item.withPrimaryKey(new PrimaryKey("id", addTeam.getId()));

        if (addTeam.getUserIds() != null) {
            item.withList("userIds", addTeam.getUserIds());
        }

        if (addTeam.getName() != null) {
            item.withString("name", addTeam.getName());
        }

        return item;
    }
}

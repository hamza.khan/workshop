package com.opsgenie.workshop.team;

import com.opsgenie.workshop.team.dto.TeamDTO;
import com.opsgenie.workshop.team.dto.TeamMembersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/teams")
public class TeamRestController {
    @Autowired
    private TeamService teamService;

    @RequestMapping(method = RequestMethod.POST)
    public TeamDTO addTeam(@RequestBody TeamDTO teamDTO) {
        Team team = teamDTO.toTeam();

        Team addedTeam = teamService.addTeam(team);

        return TeamDTO.fromEntity(addedTeam);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<TeamDTO> listTeams() {
        return teamService.listTeams().stream()
                .map(TeamDTO::fromEntity)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/{teamId}", method = RequestMethod.GET)
    public TeamDTO getTeam(@PathVariable String teamId) {
        return TeamDTO.fromEntity(teamService.getTeam(teamId));
    }

    @RequestMapping(value = "/{teamId}", method = RequestMethod.PUT)
    public ResponseEntity putTeam(@RequestBody TeamDTO teamDTO, @PathVariable String teamId) {
        Team team = teamService.getTeam(teamId);
        if (team == null)
            return ResponseEntity.notFound().build();

        Team newTeam = new Team(team);
        newTeam.setName(teamDTO.getName());
        newTeam.setUserIds(teamDTO.getUserIds());

        Team updatedTeam = teamService.updateTeam(newTeam);

        return ResponseEntity.ok(TeamDTO.fromEntity(updatedTeam));
    }

    @RequestMapping(value = "/{teamId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteTeam(@PathVariable String teamId) {
        Team deletedTeam = teamService.deleteTeam(teamId);

        if (deletedTeam == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok(TeamDTO.fromEntity(deletedTeam));
    }

    @RequestMapping(value = "/{teamId}/assignTeamMembers", method = RequestMethod.POST)
    public ResponseEntity addMembers(@PathVariable String teamId, @RequestBody TeamMembersDTO dto) {
        Team team = teamService.getTeam(teamId);
        if (team == null)
            return ResponseEntity.notFound().build();

        if (teamService.addMembers(dto.getUserIds(), teamId))
            return ResponseEntity.accepted().build();

        return ResponseEntity.status(500).build();
    }
}

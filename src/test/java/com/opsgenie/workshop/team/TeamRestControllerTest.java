package com.opsgenie.workshop.team;

import com.opsgenie.workshop.TestUtil;
import com.opsgenie.workshop.team.dto.TeamDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Collections;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamRestController.class)
public class TeamRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamService teamService;

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        when(teamService.listTeams()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/teams"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("[]")));
    }

    @Test
    public void addTeam() throws Exception {
        TeamDTO addTeamRequestDTO = new TeamDTO("test1", null, new ArrayList<String>());

        Team expectedTeam = new Team("test1", "id1", new ArrayList<>());
        TeamDTO expectedTeamDTO = TeamDTO.fromEntity(expectedTeam);

        when(teamService.addTeam(any())).thenReturn(expectedTeam);

        mockMvc.perform(post("/teams")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(addTeamRequestDTO)))
                .andExpect(status().isOk())
                .andExpect(content().bytes(TestUtil.convertObjectToJsonBytes(expectedTeamDTO)));
    }

    @Test
    public void getTeam() throws Exception {
        Team expectedTeam = new Team("test1", "1", new ArrayList<>());
        TeamDTO expectedTeamDTO = TeamDTO.fromEntity(expectedTeam);

        when(teamService.getTeam(any())).thenReturn(expectedTeam);

        mockMvc.perform(get("/teams/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().bytes(TestUtil.convertObjectToJsonBytes(expectedTeamDTO)));
    }

    @Test
    public void putTeam() throws Exception {
        TeamDTO putTeamRequestDTO = new TeamDTO("test1", null, new ArrayList<String>());

        Team expectedTeam = new Team("test1", "id1", new ArrayList<>());
        TeamDTO expectedTeamDTO = TeamDTO.fromEntity(expectedTeam);

        when(teamService.getTeam(any())).thenReturn(expectedTeam);

        when(teamService.updateTeam(any())).thenReturn(expectedTeam);

        mockMvc.perform(put("/teams/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(putTeamRequestDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().bytes(TestUtil.convertObjectToJsonBytes(expectedTeamDTO)));
    }

    @Test
    public void deleteTeam() throws Exception {
        TeamDTO deleteTeamRequestDTO = new TeamDTO("test2", null, new ArrayList<String>());

        Team expectedTeam = new Team("test2", "1", new ArrayList<>());
        TeamDTO expectedTeamDTO = TeamDTO.fromEntity(expectedTeam);

        when(teamService.getTeam(any())).thenReturn(expectedTeam);

        when(teamService.deleteTeam(any())).thenReturn(expectedTeam);

        mockMvc.perform(delete("/teams/1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(deleteTeamRequestDTO)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().bytes(TestUtil.convertObjectToJsonBytes(expectedTeamDTO)));
    }
}

package com.opsgenie.workshop.team;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.opsgenie.workshop.team.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class TeamServiceImpl implements TeamService {

    @Qualifier("TeamRepositoryDbImpl")
    @Autowired
    private TeamRepository teamRepository;

    @Value("${aws.secretKey}")
    private String secretKey;
    @Value("${aws.accessKey}")
    private String accessKey;
    @Value("${aws.queueName:onboarding_team_assign_queue}")
    private String queueName;
    @Value("${aws.region:us-east-2}")
    private String region;

    private AmazonSQS sqs;
    private String queueUrl;

    @PostConstruct
    void postConstruct() {
        AWSCredentials awsCredentials = new BasicAWSCredentials(accessKey, secretKey);
        AWSCredentialsProvider awsCredentialsProvider = new AWSStaticCredentialsProvider(awsCredentials);

        sqs = AmazonSQSClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(region)
                .build();

        queueUrl = sqs.getQueueUrl(queueName).getQueueUrl();
    }

    public List<Team> listTeams() {
        return teamRepository.listTeams();
    }

    public Team addTeam(Team team) {
        team.setId(UUID.randomUUID().toString());

        teamRepository.add(team);

        return team;
    }

    @Override
    public Team getTeam(String teamId) {
        return teamRepository.get(teamId);
    }

    @Override
    public Team deleteTeam(String deleteTeam) {
        return teamRepository.delete(deleteTeam);
    }

    @Override
    public Team updateTeam(Team newTeam) {
        return teamRepository.update(newTeam);
    }

    @Override
    public boolean addMembers(List<String> userIds, String teamId) {

        for (String userId : userIds) {
            SendMessageRequest send_msg_request = new SendMessageRequest()
                    .withQueueUrl(queueUrl)
                    .withMessageBody("{\n" +
                            "teamId: " + teamId + ",\n" +
                            "userId: " + userId + "\n" +
                            "}")
                    .withDelaySeconds(5);
            sqs.sendMessage(send_msg_request);
        }

        return true;
    }

    @Scheduled(fixedDelay = 10000)
    public void collect() {

        List<Message> messages = sqs.receiveMessage(queueUrl).getMessages();

        for (Message m : messages) {

            Map<String, String> messageMap = stringToMap(m.getBody());

            teamRepository.addMember(messageMap.get("userId"), messageMap.get("teamId"));

            sqs.deleteMessage(queueUrl, m.getReceiptHandle());
        }
    }

    public static Map<String, String> stringToMap(String value) {

        value = value.substring(1, value.length() - 1);
        String[] keyValuePairs = value.split(",");
        Map<String, String> map = new HashMap<>();

        for (String pair : keyValuePairs) {
            String[] entry = pair.split(":");
            map.put(entry[0].trim(), entry[1].trim());
        }

        return map;
    }
}

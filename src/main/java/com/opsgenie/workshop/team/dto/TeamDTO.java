package com.opsgenie.workshop.team.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.opsgenie.workshop.team.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamDTO {
    @JsonProperty("name")
    private String name;

    @JsonProperty("id")
    private String id;

    @JsonProperty("userIds")
    private List userIds;

    public TeamDTO() {

    }

    public TeamDTO(String name, String id, List userIds) {
        this.name = name;
        this.id = id;

        this.userIds = new ArrayList();

        if (userIds != null)
            this.userIds.addAll(userIds);
    }

    public static TeamDTO fromEntity(Team expectedTeam) {
        return new TeamDTO(expectedTeam.getName(), expectedTeam.getId(), expectedTeam.getUserIds());
    }

    public Team toTeam() {
        Team team = new Team();

        team.setId(getId());
        team.setName(getName());
        team.setUserIds(getUserIds());

        return team;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List getUserIds() {
        return userIds;
    }

    public void setUserIds(List userIds) {
        this.userIds = userIds;
    }
}

package com.opsgenie.workshop.team.repository;

import com.opsgenie.workshop.team.Team;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TeamRepositoryMemImpl implements TeamRepository {
    private List<Team> teams = new ArrayList<>();

    public List<Team> listTeams() {
        return teams;
    }

    @Override
    public Team add(Team team) {
        team.setId(UUID.randomUUID().toString());
        teams.add(team);

        return team;
    }

    @Override
    public Team get(String teamId) {
        for (Team team : teams) {
            if (team.getId().equals(teamId))
                return team;
        }

        return null;
    }

    @Override
    public Team delete(String deleteTeam) {
        for (Team team : teams) {
            if (team.getId().equals(deleteTeam)) {
                teams.remove(team);
                return team;
            }
        }

        return null;
    }

    @Override
    public Team update(Team newTeam) {
        for (Team oldTeam : teams) {
            if (oldTeam.getId().equals(newTeam.getId())) {
                teams.remove(oldTeam);
                teams.add(newTeam);

                return newTeam;
            }
        }

        return null;
    }

    @Override
    public Team addMember(String userId, String teamId) {
        return null;
    }
}

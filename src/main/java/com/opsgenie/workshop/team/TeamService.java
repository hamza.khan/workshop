package com.opsgenie.workshop.team;

import java.util.List;

public interface TeamService {
    List<Team> listTeams();

    Team addTeam(Team team);

    Team getTeam(String teamId);

    Team deleteTeam(String team);

    Team updateTeam(Team team);

    boolean addMembers(List<String> userIds, String teamId);
}

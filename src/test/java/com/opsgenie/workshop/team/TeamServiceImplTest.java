package com.opsgenie.workshop.team;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.opsgenie.workshop.team.repository.TeamRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceImplTest {
    @InjectMocks
    private TeamServiceImpl teamService;

    @Mock
    private TeamRepository teamRepository;

    @Mock
    private AmazonSQS sqs;

    @After
    public void tearDown() {
        teamRepository = null;
        teamService = null;
    }

    @Test
    public void listTeams() {
        List<Team> expectedTeams = new ArrayList<>();

        Team team = new Team();
        expectedTeams.add(team);

        team = new Team();
        expectedTeams.add(team);

        when(teamRepository.listTeams()).thenReturn(expectedTeams);

        final List<Team> teams = teamService.listTeams();

        assertThat(teams, is(equalTo(expectedTeams)));
    }

    @Test
    public void addTeam() {
        Team expectedTeam = new Team();

        when(teamRepository.add(any())).thenReturn(expectedTeam);

        assertThat(expectedTeam, is(equalTo(teamService.addTeam(expectedTeam))));
    }

    @Test
    public void getTeam() {
        Team expectedTeam = new Team();

        when(teamRepository.get(any())).thenReturn(expectedTeam);

        assertThat(expectedTeam, is(equalTo(teamService.getTeam("1"))));
    }

    @Test
    public void deleteTeam() {
        Team expectedTeam = new Team();

        when(teamRepository.delete(any())).thenReturn(expectedTeam);

        assertThat(expectedTeam, is(equalTo(teamService.deleteTeam(expectedTeam.getId()))));
    }

    @Test
    public void updateTeam() {
        Team expectedTeam = new Team();

        when(teamRepository.update(any())).thenReturn(expectedTeam);

        assertThat(expectedTeam, is(equalTo(teamService.updateTeam(expectedTeam))));
    }

    @Test
    public void addMembersWithEmptyUserIds() {
        teamService.addMembers(Collections.emptyList(), "team1");
        verifyZeroInteractions(sqs);
    }

    @Test
    public void addMembersWithMultipleUserIds() {
        String teamId = "teamId";
        List<String> userIds = Arrays.asList("user1", "user2");

        ArgumentCaptor<SendMessageRequest> captor = ArgumentCaptor.forClass(SendMessageRequest.class);

        teamService.addMembers(userIds, teamId);

        verify(sqs, times(2)).sendMessage(captor.capture());

        List<SendMessageRequest> allValues = captor.getAllValues();

        for (int i = 0; i < userIds.size(); i++) {
            String userId = userIds.get(i);
            assertThat(allValues.get(i).getMessageBody(), is(equalTo("{\n" +
                    "teamId: " + teamId + ",\n" +
                    "userId: " + userId + "\n" +
                    "}")));
        }
    }
}


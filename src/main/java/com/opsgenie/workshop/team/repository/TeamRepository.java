package com.opsgenie.workshop.team.repository;

import com.opsgenie.workshop.team.Team;

import java.util.List;

public interface TeamRepository {
    List<Team> listTeams();

    Team add(Team team);

    Team delete(String team);

    Team get(String id);

    Team update(Team team);

    Team addMember(String userId, String teamId);
}

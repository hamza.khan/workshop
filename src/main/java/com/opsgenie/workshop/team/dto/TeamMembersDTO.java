package com.opsgenie.workshop.team.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class TeamMembersDTO {

    @JsonProperty
    List<String> userIds;

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }
}

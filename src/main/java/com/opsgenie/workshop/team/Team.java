package com.opsgenie.workshop.team;

import java.util.ArrayList;
import java.util.List;

public class Team {
    private String name;
    private String id;
    private List<String> userIds;

    public Team(String name, String id, List<String> userIds) {
        this.name = name;
        this.id = id;

        this.userIds = new ArrayList<>();
        if (userIds != null)
            this.userIds.addAll(userIds);
    }

    public Team() {
        this.userIds = new ArrayList<>();
    }

    public Team(Team team) {
        this.id = team.getId();
        this.name = team.getName();
        this.userIds = team.getUserIds();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {

        this.userIds = userIds;
    }
}
